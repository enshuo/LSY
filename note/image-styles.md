Ref:https://gist.github.com/sskylar/2faedc4ac82aef2631b9#file-image-styles-md

Here are two ways to style images using Markdown

## 1. Using class names

If you'd like to use classes, HTML is completely valid in Markdown:

```html
<span class="right">
![](/assets/image.jpg) 
</span>
```

In CSS you can then target:

```css
.right img { float: right; }
```

Easy!

## 2. Using Markdown syntax

For something a little cleaner, you could also take advantage of Markdown syntax as a shortcut. For example, here's how you could get 3 different image styles using just Markdown and CSS:

```markdown
![](/assets/image.jpg) 

*![](/assets/image.jpg)*

**![](/assets/image.jpg)**
```

This would render as:

```html
<img src="/assets/image.jpg">

<em><img src="/assets/image.jpg"></em>

<strong><img src="/assets/image.jpg"></strong>
```

Then you could target in CSS using:

```css
img { /* default */ }

em > img { float: right; }

strong > img { width: 100%; }
```